#!/bin/bash


#SBATCH -o slurm-gpu-job-.out
#SBATCH -p gpu-nodes
#SBATCH --gres=gpu:1

srun --gres=gpu:1 slurmrun.sh $@

wait
