#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 19:12:38 2018

@author: unlearn
"""

import numpy as np
import cv2
from sklearn import preprocessing
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt



def check(classifier, input_size, image_name, cov_matrix):
    test_image = cv2.imread(image_name)
    print(test_image.shape)
    test_image = cv2.cvtColor(test_image, cv2.COLOR_BGR2GRAY)
    print(test_image.shape)
    test_image = get_square(test_image,input_size[0])
    test_image_vec = np.reshape(test_image,(len(test_image)*len(test_image[0])),1)
    test_image_vec = np.reshape(test_image_vec,(1,-1))
    print(test_image_vec.shape)
    test_input = np.matmul(test_image_vec,cov_matrix)
    test_input = preprocessing.StandardScaler().fit_transform(test_input)
    print(test_input.shape)
    y_pred = classifier.predict(test_input)
    y_pred2 = np.zeros((len(y_pred),len(y_pred[0])))
    for i in range(len(y_pred)):
        y_pred2[i][np.argmax(y_pred[i])] = 1
    print(y_pred2)
    
    



def get_square(image,square_size):

    height,width=image.shape
    if(height>width):
      differ=height
    else:
      differ=width
    differ+=4

    mask = np.zeros((differ,differ), dtype="uint8")   
    x_pos=int((differ-width)/2)
    y_pos=int((differ-height)/2)
    mask[y_pos:y_pos+height,x_pos:x_pos+width]=image[0:height,0:width]
    mask=cv2.resize(image,(square_size,square_size),interpolation=cv2.INTER_LANCZOS4)

    return mask

def check_compression(data, pca):
    for image in data[1:10]:
        plt.title("1")
        plt.imshow(image.reshape(80,80))
        plt.show()
        plot = pca.transform(image.reshape(1,-1))
        image_red = pca.inverse_transform(plot)
        plt.title("2")
        plt.imshow(image_red.reshape(80,80))
        plt.show()