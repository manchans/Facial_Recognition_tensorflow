#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 14:26:44 2018

@author: manchans
"""
    
import os, zipfile
import bz2
from bz2 import decompress

dir_name = '/home/unlearn/Downloads/colorferet/colorferet/dvd2/data/images'
extension = ".bz2"

os.chdir(dir_name) # change directory from working dir to dir with files

counter=0
for folder in os.listdir(dir_name):
    print(folder)
    counter = 0
    os.chdir(dir_name + '/' + folder)
    for item in os.listdir(dir_name + '/' + folder): # loop through items in dir
        if item.endswith(extension): # check for ".zip" extension
            file_name = os.path.abspath(item) # get full path of files
            print(file_name)
            ccc = bz2.BZ2Decompressor()
            data = open(file_name, "rb").read()
            data = ccc.decompress(data)
            with open(str(counter), "wb") as newfile:
                newfile.write(data)
            counter = counter+1
            os.remove(file_name) # delete zipped file
    