#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 10 14:47:18 2018

@author: manchans
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
import os

parent = os.path.abspath(os.path.join(os.getcwd(), os.pardir))

def fourier_transform(img):
    f = np.fft.fft2(img)
    fshift = np.fft.fftshift(f)
    magnitude_spectrum = 20*np.log(np.abs(fshift))
    
    plt.subplot(121),plt.imshow(img, cmap = 'gray')
    plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(magnitude_spectrum, cmap = 'gray')
    plt.title('Magnitude Spectrum'), plt.xticks([]), plt.yticks([])
    plt.show()
    print((magnitude_spectrum))
    print((img))
    
    
def make_fourier_dataset(path):
    rootdir = path
    for subdir, dirs, files in os.walk(rootdir):
        print(type(files))
        for file1 in files:
            file_path = os.path.join(subdir, file1)
            if(os.path.isfile(file_path) and file_path.endswith('jpg')):
                img = cv2.imread(file_path)
                f = np.fft.fft2(img)
                fshift = np.fft.fftshift(f)
                magnitude_spectrum = 20*np.log(np.abs(fshift))
                print(subdir)
                if not os.path.isdir(parent + '/Dataset/faces95_exp_fourier/'+subdir.split('/')[-1]):
                    os.makedirs(parent + '/Dataset/faces95_exp_fourier/'+subdir.split('/')[-1])
                cv2.imwrite(parent+'/Dataset/faces95_exp_fourier/'+subdir.split('/')[-1] + '/' + str(file1),magnitude_spectrum)
                        
                
    
    
    



image = cv2.imread(parent + '/Dataset/faces95_exp/adhast/adhast.1.jpg',0)
fourier_transform(image)

image = cv2.imread(parent + '/Dataset/faces95_exp/ajbake/ajbake.2.jpg',0)
fourier_transform(image)

make_fourier_dataset(parent + '/Dataset/faces95_exp')