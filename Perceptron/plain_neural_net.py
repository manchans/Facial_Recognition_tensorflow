#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  8 12:24:04 2018

@author: unlearn
"""

from keras.models import Sequential
from keras.layers import Dense
import numpy as np 
from keras.layers import Dropout
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense, BatchNormalization, Activation


def build_model(input_dim1, output_dim):
    print(input_dim1)
    print(output_dim)
    model = Sequential()
    #model.add(Dense(1000, , activation = 'relu'))
    #model.add(Dense(512, activation = 'relu'))
    model.add(Dense(1000,input_dim=input_dim1,activation = 'selu', kernel_initializer = 'random_normal'))
#    model.add(Dropout(0.2))
#    model.add(BatchNormalization())
    model.add(Dense(800,activation = 'selu', kernel_initializer = 'random_normal'))
#    model.add(Dropout(0.2))
#    model.add(BatchNormalization())
    model.add(Dense(500,activation = 'selu', kernel_initializer = 'random_normal'))
    
#    model.add(BatchNormalization())
    model.add(Dense(250,activation = 'selu', kernel_initializer = 'random_normal'))
    
#    model.add(Dropout(0.2))
#    model.add(BatchNormalization())
    model.add(Dense(output_dim, activation = 'softmax'))
    return model


def build_conv_model():
# Initialising the CNN
    classifier = Sequential()
    
    # Step 1 - Convolution
    classifier.add(Conv2D(64, (5, 5), input_shape = (149,149,3), kernel_initializer = 'glorot_normal'))
#    classifier.add(BatchNormalization())
    classifier.add(Activation('relu'))
    # Step 2 - Pooling
    
    # Adding a second convolutional layer
    classifier.add(Conv2D(64, (5, 5), kernel_initializer = 'glorot_normal'))
#    classifier.add(BatchNormalization())
    classifier.add(Activation('relu'))
    
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    
    classifier.add(BatchNormalization())
    
    classifier.add(Conv2D(64, (5, 5), kernel_initializer = 'glorot_normal'))
#    classifier.add(BatchNormalization())
    classifier.add(Activation('relu'))
    # Step 2 - Pooling
    
    # Adding a second convolutional layer
    classifier.add(Conv2D(64, (5, 5), kernel_initializer = 'glorot_normal'))
#    classifier.add(BatchNormalization())
    classifier.add(Activation('relu'))
    
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    
    classifier.add(BatchNormalization())

    classifier.add(Conv2D(64, (5, 5), kernel_initializer = 'glorot_normal'))
#    classifier.add(BatchNormalization())
    classifier.add(Activation('relu'))
    # Step 2 - Pooling
    
    # Adding a second convolutional layer
    classifier.add(Conv2D(64, (5, 5), kernel_initializer = 'glorot_normal'))
#    classifier.add(BatchNormalization())
    classifier.add(Activation('relu'))
    
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    
#    classifier.add(Conv2D(64, (3, 3), kernel_initializer = 'glorot_normal'))
##    classifier.add(BatchNormalization())
#    classifier.add(Activation('relu'))
#    
#    classifier.add(MaxPooling2D(pool_size = (2, 2)))
#    
#    classifier.add(Conv2D(64, (3, 3), kernel_initializer = 'glorot_normal'))
##    classifier.add(BatchNormalization())
#    classifier.add(Activation('relu'))
#    
#    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    
    
    # Step 3 - Flattening
    classifier.add(Flatten())
    
    # Step 4 - Full connection
#    classifier.add(Dense(units = 256, activation = 'relu', kernel_initializer = 'glorot_normal'))
    classifier.add(Dense(units = 373, activation = 'softmax', kernel_initializer = 'glorot_normal'))
    return classifier
    
