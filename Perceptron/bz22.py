#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 15:02:20 2018

@author: manchans
"""

import sys
import os
import bz2
from bz2 import decompress

path = "/users/pgrad/manchans/Downloads/colorferet/colorferet/dvd1/data/images"
for(dirpath,dirnames,files)in os.walk(path):
   for filename in files:
       filepath = os.path.join(dirpath,filename)
       newfile = bz2.decompress(filename)