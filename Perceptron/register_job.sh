#!/bin/bash

name=`date --iso-8601=seconds`
sbatch --job-name=$name.vrun --output=$name.out slurm-gpu-job.sh $@
