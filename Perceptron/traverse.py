#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 13:13:21 2018

@author: manchans
"""

#Pre-process images using openCV
import os
import cv2
import os.path
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn import preprocessing
from sklearn.preprocessing import OneHotEncoder
from plain_neural_net import build_model, build_conv_model
import numpy as np
from scipy import linalg as LA
from sklearn import decomposition
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import tensorflow as tf
from tensorflow.python.tools import freeze_graph, optimize_for_inference_lib
from keras import backend as K
import math
from sklearn import preprocessing
from FileIO import write_conv, read_cov
from Validate import check, get_square
from PIL import Image
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from Validate import check_compression
from ImageManipulations import crop_images, get_square, subtract_avg, convert_to_grayscale
from transformation import transform_lda, pca_sklearn
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from transformation import transform_auto_encoder
from ConvEncoder import encode

MODEL_NAME = 'regression_model'



parent = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
all_datasets = list()
dataset_pre = parent + '/Dataset/Indian Cricket Team/indian cricket team'
all_datasets.append(dataset_pre)
dataset_path = parent + '/Dataset/Indian Cricket Team/Train'
all_datasets.append(dataset_path)
yale = parent + '/Dataset/yalefaces_dataset'
all_datasets.append(yale)
#cricket = parent + '/Dataset/Indian Cricket Team/Train'
#all_datasets.append()
essex = parent + '/Dataset/essex/all_faces'
all_datasets.append(essex)
feret_dvd1_uncropped = '/home/unlearn/Downloads/colorferet/colorferet/dvd1/data/images'
all_datasets.append(feret_dvd1_uncropped)
feret_dvd2_uncropped = '/home/unlearn/Downloads/colorferet/colorferet/dvd2/data/images'
all_datasets.append(feret_dvd2_uncropped)
arun = '/home/unlearn/Desktop/Facial_Recognition_tensorflow/Dataset/sample-photos/train'
feret_dvd2 = parent + '/Dataset/FERET/dvd2'
feret_dvd1 = parent + '/Dataset/FERET/dvd1/images'
feret = '/users/pgrad/manchans/Desktop/Facial Recognition Tensorflow/Dataset/FERET/all'

dataset_pre = essex
dataset_path = essex
dataset = list()
labels = list()
dt = np.dtype(float)
largest = (0,0)
smallest = (0,0) 
average = (0,0)
SIZE = 80
train_average_face = np.zeros((SIZE,SIZE)) 
class_label = list()
sc = StandardScaler()
debug = False


    
    
def produce_dataset():
    path = parent + '/Dataset/essex/all_faces'
    average = convert_to_grayscale(path, SIZE)
    subtract_avg(path, average)

def prepare_instances(path):
    global output
    global class_label
    x_temp = list()
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
            #print(subdir.split('/')[-1])
            try:
                file_path = os.path.join(subdir, file1)
                if debug:
                    print(file_path)
                if(os.path.isfile(file_path)):
                    image = cv2.imread(file_path)
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    image = get_square(image,SIZE)
#                    image = cv2.resize(image, smallest) 
                    if debug:
                        print(image.shape)
                    image_vec = np.reshape(image,(len(image)*len(image[0])))
                    if debug:
                        print(image_vec.shape)
                    x_temp.append(image_vec.tolist())
                    if debug:
                        print('sahil')
                        print(subdir.split('/')[-1])
                    labels.append(subdir.split('/')[-1])
                    if subdir.split('/')[-1] not in class_label:
                        class_label.append(subdir.split('/')[-1])
            except:
                print("fail")
                continue
    output = pd.get_dummies(labels)
    x = np.asarray(x_temp)
    if debug:
        print(x.shape)
        print(len(labels))
    return (x,output)

def prepare__test_instances(path):
    x_temp = list()
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
            print(file1)
            try:
                file_path = os.path.join(subdir, file1)
                print(file_path)
                if(os.path.isfile(file_path) and file_path.endswith('jpg')):
                    image = cv2.imread(file_path)
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    image = get_square(image,SIZE)
#                    image = cv2.resize(image, smallest) 
                    print(image.shape)
                    image_vec = np.reshape(image,(len(image)*len(image[0])))
                    print(image_vec.shape)
                    x_temp.append(image_vec.tolist())
            except:
                continue
    x = np.asarray(x_temp)
    print(x.shape)
    return x
        
def covariance_matrix(d):
    pca = PCA(10000)
    print(pca)
    d_new = pca.fit(d)
    d_new = d_new.fit_transform(d)
    print('d_new shape')
    print(d_new.shape)
    return d_new   
    

    
def train(reduced_data, out):
    _,o = out.shape
    e,i = reduced_data.shape
    classifier = build_model(i,o)
    print(classifier.summary())
    classifier.compile(optimizer='Adadelta',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    
#    X_train, X_test, y_train, y_test = train_test_split(reduced_data, out, test_size=0.2)
    if debug:
        print("feeding into the net")
        print(out.shape)
        print(reduced_data.shape)
    classifier.fit(reduced_data,out,epochs=10, steps_per_epoch=100)
#    y_pred = classifier.predict(X_test)
#    y_pred2 = np.zeros((len(y_pred),len(y_pred[0])))
#    for i in range(len(y_pred)):
#        y_pred2[i][np.argmax(y_pred[i])] = 1
#        
#    report = classification_report(y_test, y_pred2)
        
#    print(report)
    return classifier

def train_convolution_net():
    data = prepare_instances(parent+'/Dataset/essex/all_faces')
    print('data from prepared instances')
    print(data.shape)
    output = pd.get_dummies(labels)
    X_train, X_test, y_train, y_test = train_test_split(data, output, test_size=0.2)
    classifier = build_conv_model()
    classifier.compile(optimizer='adagrad',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    classifier.fit(X_train,y_train,epochs=300, steps_per_epoch=200)
    

def export_model(saver, model, input_node_names, output_node_name):
    tf.train.write_graph(K.get_session().graph_def, 'out', \
        MODEL_NAME + '_graph.pbtxt')

    saver.save(K.get_session(), 'out/' + MODEL_NAME + '.chkp')

    freeze_graph.freeze_graph('out/' + MODEL_NAME + '_graph.pbtxt', None, \
        False, 'out/' + MODEL_NAME + '.chkp', output_node_name, \
        "save/restore_all", "save/Const:0", \
        'out/frozen_' + MODEL_NAME + '.pb', True, "")

    input_graph_def = tf.GraphDef()
    with tf.gfile.Open('out/frozen_' + MODEL_NAME + '.pb', "rb") as f:
        input_graph_def.ParseFromString(f.read())

    output_graph_def = optimize_for_inference_lib.optimize_for_inference(
            input_graph_def, input_node_names, [ output_node_name],
            tf.float32.as_datatype_enum)

    with tf.gfile.FastGFile('out/opt_' + MODEL_NAME + '.pb', "wb") as f:
        f.write(output_graph_def.SerializeToString())

    print("graph saved!")
    



def find_extremes(path):
    global largest
    global smallest
    global average
    minx = 1000
    miny = 1000
    maxx = 0
    maxy = 0
    counter = 0
    totalx = 0
    totaly = 0
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
            try:
                file_path = os.path.join(subdir, file1)
                if(os.path.isfile(file_path)):
                    image = cv2.imread(file_path) 
                    totalx += len(image)
                    totaly += len(image[0])
                    counter += 1
                    if(len(image)<minx):
                        minx = len(image)
                    if(len(image[0])<miny):
                        miny = len(image[0])
                    if(len(image)>maxx):
                        maxx = len(image)
                    if(len(image[0])>maxy):
                        maxy = len(image[0])
            except:
                continue
    largest = (maxx,maxy)
    smallest = (minx,miny)
    average = (totalx/counter,totaly/counter)
    
                
                
def read_data(path):
    find_extremes(path)
    data,output = prepare_instances(path)
#    data_pre,_ = prepare_instances(dataset_pre)
    print('data from prepared instances')
    print(data.shape)
    fig = plt.figure()
    for i in range(1,9):    
        ax1 = fig.add_subplot(2,4,i)
        ax1.imshow(data[i*5].reshape(SIZE,SIZE))
#    ax2 = fig.add_subplot(2,2,2)
#    ax2.imshow(data[0].reshape(80,80))
#    ax3 = fig.add_subplot(2,2,3)
#    ax3.imshow(data[0].reshape(80,80))
#    ax4 = fig.add_subplot(2,2,4)
#    ax4.imshow(data[0].reshape(80,80))
#    plt.imshow(data[0].reshape(80,80))
    plt.savefig('myfig1')
    plt.show()
    if debug:
        print(output.shape)
        print('data from prepared instances after standard scalar')
        print(data.shape)
        for one in data:
            plt.imshow(one.reshape(SIZE,SIZE))
    plt.savefig('myfig2')
    plt.show()
    data = sc.fit_transform(data)
    return (data,output)

def process_data(data, output):
    le = LabelEncoder()
    labels_int = le.fit_transform(labels)
    X_train, X_cv, y_train, y_cv = train_test_split(data, labels_int, test_size=0.3)
#    reduced_data,test_input = pca_sklearn(data,data, data)
#    X_train, X_cv, y_train, y_cv = train_test_split(data, labels_int, test_size=0.2)
#    reduced_data,test_input = pca_sklearn(X_train,data, X_cv)
#    reduced_data, test_input = transform_lda(data,labels_int,X_train,X_cv)
    reduced_data, test_input, cl = transform_auto_encoder(data,np.array(labels),X_train,X_cv)
    print(cl.summary())
    if debug:
        print("labels")
        print(labels_int)
        print("y_train")
        print(y_train)
    ohe = OneHotEncoder() 
    ohe.fit(labels_int.reshape(-1,1))
    y_train = ohe.transform(y_train.reshape(-1,1)).toarray()
    y_cv = ohe.transform(y_cv.reshape(-1,1)).toarray()

    return (data,X_cv,y_cv,reduced_data,y_train, test_input)

def read_test(test_path):
    convert_to_grayscale(test_path)
    subtract_avg(test_path,train_average_face)
    x = prepare__test_instances(test_path)
    return x


def feedPerceptron(input_vec, output_vec, test_input, test_output):
    classifier = train(input_vec, output_vec)
    y_pred = classifier.predict(test_input)
    y_pred2 = np.zeros((len(y_pred),len(y_pred[0])))
    for i in range(len(y_pred)):
            y_pred2[i][np.argmax(y_pred[i])] = 1
            
    report = classification_report(test_output, y_pred2)
    print(report)
    return classifier
            

if __name__ == "__main__":
    find_extremes(dataset_path)
#    crop_images(dataset_path)
#    train_average_face = convert_to_grayscale(dataset_path)
#    subtract_avg(dataset_path,train_average_face)
    one, two = read_data(dataset_path)
    data, x_cv, y_cv, red_data, out, test_input = process_data(one,two)
#    if debug:
#        print("gef iu")
#        print(red_data.shape)
    classifier = feedPerceptron(red_data, out, test_input, y_cv)
#    export_model(tf.train.Saver(), classifier, ["dense_1_input"], "dense_5/Softmax")
    
#    check(classifier, smallest, 'cv.jpg')
#    
#    x_new = read_test(parent + '/Dataset/Indian Cricket Team/Test/Indian Cricket team group photo')
#    test_input = np.matmul(x_new,cov_new)
#    test_input = preprocessing.StandardScaler().fit_transform(test_input)
#    y_pred = classifier.predict(test_input)
#    y_pred2 = np.zeros((len(y_pred),len(y_pred[0])))
#    for i in range(len(y_pred)):
#            y_pred2[i][np.argmax(y_pred[i])] = 1
#            
#    for i in range(len(y_pred2)):
#        print(class_label[np.argmax(y_pred2[i])])







    


    

    

