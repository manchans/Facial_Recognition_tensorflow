from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense, BatchNormalization, Activation
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
import os
from traverse import crop_images, produce_dataset, convert_to_grayscale, subtract_avg
from plain_neural_net import build_conv_model

K.set_image_dim_ordering('tf')

parent = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
MODEL_NAME = 'regression_model'

input_size = (149, 149)




#average = convert_to_grayscale(parent + '/Dataset/essex/all_faces')
#subtract_avg(parent + '/Dataset/essex/all_faces', average)
# Compiling the CNN
classifier = build_conv_model()
classifier.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])

# Part 2 - Fitting the CNN to the images


shift = 0.2
train_datagen = ImageDataGenerator(rescale = 1./255,
                                   horizontal_flip = False,
                                   width_shift_range=shift, height_shift_range=shift, zoom_range = 0.2)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory(parent + '/Dataset/essex/all_faces',
                                                 target_size = input_size,
                                                 batch_size = 32,
                                                 class_mode = 'categorical')


classifier.fit_generator(training_set,
                         steps_per_epoch = 40,
                         epochs = 50,
                         #validation_data = test_set,
                         validation_steps = 2000)

import numpy as np
from keras.preprocessing import image
test_image = image.load_img('cv2.jpg', target_size = input_size)
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = classifier.predict(test_image)
#training_set.class_indices
#if result[0][0] == 1:
#    prediction = 'saquib'
#else:
#    prediction = 'sahil'

import tensorflow as tf
from tensorflow.python.tools import freeze_graph, optimize_for_inference_lib

    
def export_model(saver, model, input_node_names, output_node_name):
    tf.train.write_graph(K.get_session().graph_def, 'out', \
        MODEL_NAME + '_graph.pbtxt')

    saver.save(K.get_session(), 'out/' + MODEL_NAME + '.chkp')

    freeze_graph.freeze_graph('out/' + MODEL_NAME + '_graph.pbtxt', None, \
        False, 'out/' + MODEL_NAME + '.chkp', output_node_name, \
        "save/restore_all", "save/Const:0", \
        'out/frozen_' + MODEL_NAME + '.pb', True, "")

    input_graph_def = tf.GraphDef()
    with tf.gfile.Open('out/frozen_' + MODEL_NAME + '.pb', "rb") as f:
        input_graph_def.ParseFromString(f.read())

    output_graph_def = optimize_for_inference_lib.optimize_for_inference(
            input_graph_def, input_node_names, [output_node_name],
            tf.float32.as_datatype_enum)

    with tf.gfile.FastGFile('out/opt_' + MODEL_NAME + '.pb', "wb") as f:
        f.write(output_graph_def.SerializeToString())

print("graph saved!")


export_model(tf.train.Saver(), classifier, ["conv2d_17_input"], "dense_5/Softmax")
