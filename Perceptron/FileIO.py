#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 27 09:57:27 2018

@author: unlearn
"""
import numpy as np
import math

def read_cov(file_name):
    matrix = list()
    temp = open(file_name,'r').read().split('\n')
    for each_line in temp:
        x = np.fromstring(each_line, dtype=np.float, sep=',' )
        matrix.append(x)
    return np.array(matrix[:-1])


def write_conv(cov_matrix,file_name):
    cov_matrix = cov_matrix.T
    new_arr = np.empty_like(cov_matrix)
    for i in range(len(cov_matrix)):
        for j in range(len(cov_matrix[i])):
            new_arr[i][j] = math.ceil(cov_matrix[i][j] * 10000.0) / 10000.0
    pc_file = open(file_name, 'w')
    for row in cov_matrix:
        for col in row:
            pc_file.write(str(format(col, '.4f')))
            pc_file.write(',')
        pc_file.write('\n')
    

        
    
m  = read_cov('principal_components.txt')
m = np.array(m)
    
    
    