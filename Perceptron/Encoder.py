#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 16:40:57 2018

@author: unlearn
"""

from keras.layers import Input, Dense, BatchNormalization
from keras.models import Model
from keras.datasets import mnist
from keras.models import Sequential
import numpy as np
SIZE = 80

def encode(input_dim, output_dim, X_train, X_cv):
    print(X_train[0])
    input_img = Input(shape = (input_dim,))
    hidden_in_0 = Dense(int(input_dim-200), activation='relu', kernel_initializer='glorot_normal')(input_img)
    Batch_norm_1 = BatchNormalization()(hidden_in_0)
#    hidden_in_1 = Dense(int(input_dim-700), activation='relu', kernel_initializer='glorot_normal')(Batch_norm_1)
#    Batch_norm_2 = BatchNormalization()(hidden_in_1)
    hidden_in_2 = Dense(int(input_dim-1000), activation = 'relu',kernel_initializer='glorot_normal')(Batch_norm_1)
    Batch_norm_3 = BatchNormalization()(hidden_in_2)
    encoded = Dense(output_dim, activation='relu',kernel_initializer='glorot_normal')(Batch_norm_3)
    Batch_norm_4 = BatchNormalization()(encoded)
    hidden_out_1 = Dense(int(input_dim-1000), activation='relu', kernel_initializer='glorot_normal')(Batch_norm_4)
    Batch_norm_5 = BatchNormalization()(hidden_out_1)
#    hidden_out_2 = Dense(int(input_dim-700), activation='relu', kernel_initializer='glorot_normal')(Batch_norm_5)
#    Batch_norm_6 = BatchNormalization()(hidden_out_2)
    hidden_out_0 = Dense(int(input_dim-200), activation='relu', kernel_initializer='glorot_normal')(Batch_norm_5)
    Batch_norm_7 = BatchNormalization()(hidden_out_0)
    decoded = Dense(input_dim, activation='relu', kernel_initializer='glorot_normal')(Batch_norm_7)
    autoencoder = Model(input_img, decoded)
    encoder = Model(input_img, encoded)
##    encoded_input = Input(shape=(output_dim,))
#    # retrieve the last layer of the autoencoder model
#    mid_layer = autoencoder.layers[-2]
#    decoder_layer = autoencoder.layers[-1]
#    # create the decoder model
#    decoder = Sequential()
#    decoder.add(mid_layer)
#    decoder.add(decoder_layer)
    
#    x = hidden_2(encoded_input)
#    x = mid_layer(x)
#    out = decoder_layer(x)
#    decoder = Model(inputs=x, outputs=out)
    autoencoder.compile(optimizer='Adadelta', loss='mean_squared_error')
    X_train = X_train.reshape((len(X_train), np.prod(X_train.shape[1:])))
    X_cv = X_cv.reshape((len(X_cv), np.prod(X_cv.shape[1:])))
    print(X_train.shape)
    print(X_cv.shape)
    autoencoder.fit(X_train, X_train,
                epochs=80,
                batch_size=256,
                shuffle=True,
                validation_data=(X_cv, X_cv))
    reduced_Data = encoder.predict(X_train)
    encoded_imgs = encoder.predict(X_cv)
    print(encoded_imgs.shape)
    decoded_imgs = autoencoder.predict(X_cv)
    import matplotlib.pyplot as plt

    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(2, n, i + 1)
        plt.imshow(X_cv[i].reshape(SIZE, SIZE))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    
        # display reconstruction
        ax = plt.subplot(2, n, i + 1 + n)
        plt.imshow(decoded_imgs[i].reshape(SIZE, SIZE))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()
    return (reduced_Data,encoded_imgs, autoencoder)