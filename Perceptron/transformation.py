#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  4 14:02:58 2018

@author: unlearn
"""

import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from FileIO import write_conv, read_cov
from Encoder import encode
import os
import cv2
from PIL import Image
import imageio

SIZE = 80

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

debug = True
def transform_lda(X,Y, x_train,x_cv):
    clf = LinearDiscriminantAnalysis()
    clf.fit(X,Y)
    transformed = clf.transform(x_train)
    test_input = clf.transform(x_cv)
    print("before lda")
    print(X.shape)
    print("lda")
    print(transformed.shape)
    return (transformed, test_input)

def pca_sklearn(data, pre_data, x_cv):
    print(data.shape)
    pca = PCA(n_components = 35*35)
    pca.fit(pre_data)
#    check_compression(pre_data,pca)
#    pca.fit(pre_data)
    image_dem = pre_data[0]
    X = pca.transform(data)
    image_dem = pca.transform(image_dem.reshape(1,-1))
    print('shape of x')
    print(X.shape)
    principal_components = pca.components_
    temp = pca.inverse_transform(image_dem)
    image = plt.imshow(temp.reshape(SIZE,SIZE))
    plt.savefig('myfig_red')
    plt.show()
    total_variance = 0
    counter = 0
    for i in pca.explained_variance_ratio_:
         total_variance += i
         counter += 1
         if(total_variance>0.98):
             break
    print(total_variance)
    print('principal components found out')
    print(counter)
    write_conv(principal_components,'principal_components.txt')
    fig = plt.figure(figsize=(8,8))
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)
    for i in range(10):
        ax = fig.add_subplot(5, 5, i+1, xticks=[], yticks=[])
        ax.imshow(np.reshape(principal_components[i,:], (SIZE,SIZE)), cmap=plt.cm.bone, interpolation='nearest')
    if debug:
        print('principal components shape')
        print(principal_components.shape)
        print('reduced data')
        print(X.shape)
        
    cov_new = read_cov('principal_components.txt')
    test_input = np.matmul(x_cv,cov_new)
    return (X, test_input)

def transform_auto_encoder(data,labels,X_train,X_cv):
    return encode(int(X_train[0].shape[0]),35*35,X_train,X_cv)

def discrete_cosine_transform(path):  # convert back
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
#print(subdir.split('/')[-1])
            file_path = os.path.join(subdir, file1)
            try:
                print(file_path)
                if(os.path.isfile(file_path)):
                    img = cv2.imread(file_path)
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    ret,thresholded = cv2.threshold(gray,200,255,cv2.THRESH_BINARY)
    
                    img = cv2.cvtColor(thresholded, cv2.COLOR_GRAY2BGR)
                    gray = thresholded
                    gray = np.float32(gray)/255.0
                    dst = cv2.dct(gray)
                    os.remove(file_path)
                    img = np.uint8(dst)*255.0
                    cv2.imwrite(file_path + '.jpg',img)
            except:
                print("fail")
                continue

def fourier_transform(path):
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
#print(subdir.split('/')[-1])
            file_path = os.path.join(subdir, file1)
            if(os.path.isfile(file_path)):
                img = cv2.imread(file_path)
                img_data = np.asarray(img)
                # perform the 2-D fast Fourier transform on the image data
                fourier = np.fft.fft2(img_data)
                # move the zero-frequency component to the center of the Fourier spectrum
                fourier = np.fft.ifftshift(fourier)
                magnitude_spectrum = 20*np.log(np.abs(fourier))
                # compute the magnitudes (absolute values) of the complex numbers
                fourier = np.abs(fourier)
                # compute the common logarithm of each value to reduce the dynamic range
                fourier = np.log10(fourier)
                # find the minimum value that is a finite number
                lowest = np.nanmin(fourier[np.isfinite(fourier)])
                # find the maximum value that is a finite number
                highest = np.nanmax(fourier[np.isfinite(fourier)])
                # calculate the original contrast range
                original_range = highest - lowest
                # normalize the Fourier image data ("stretch" the contrast)
                norm_fourier = (fourier - lowest) / original_range * 255
                # convert the normalized data into an image
#                norm_fourier_img = Image.fromarray(norm_fourier)
                cv2.imwrite(file_path + '.jpg',magnitude_spectrum)
                os.remove(file_path)
                

def rename(path):
    counter = 0
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
            file_path = os.path.join(subdir, file1)
            print(subdir)
            os.rename(file_path, subdir + '/' + str(counter))
            counter = counter+1
            
            
    