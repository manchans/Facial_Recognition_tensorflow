#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 12:57:31 2018

@author: manchans
"""
import re
import json
from traverse import crop_images
records = list()


def query_people(people_list, limit):
    for person in people_list:
        records.append({"keywords":person, "limit" : limit})
    return {"records":records}
        

arg_file = open("IndianCricketTeam","r")
name_list = list()
bands = list()

delimiters = "-","("
pattern = '|'.join(map(re.escape,delimiters))
for line in arg_file:
    if len(line)>1:
        line = line.rstrip()
        if ":" not in line:
            name_list.append(re.split(pattern,line)[0])
        else:
            bands.append(line[:-2])

       

config = open('config.json','w')
with open('result.json', 'w') as fp:
    json.dump(query_people(name_list, 70), config)
    
    
one_arg = list()
one_arg.append({"keywords":"indian cricket team", "limit" : 1500})

    
from google_images_download import google_images_download   #importing the library

response = google_images_download.googleimagesdownload()
for i in one_arg:
    paths = response.download(i)   #passing the arguments to the function
    print(paths)


    
        

        