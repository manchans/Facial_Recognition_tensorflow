#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 14:36:30 2018

@author: manchans
"""
import numpy as np
from sklearn import decomposition
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

class Covariance:
    
    input_matrix = None
    red_dimensions = 0
    
    def __init__(self, input_matrix,red_dimensions):
        self.input_matrix = input_matrix
        self.red_dimensions = red_dimensions
        
    
        
    def pca_sklearn(self):
        pca = decomposition.PCA(n_components=625)
        matrix_std = StandardScaler().fit_transform(self.input_matrix)
        y = pca.fit_transform(matrix_std)
        principal_components = pca.components_
        print(y.shape)
        return principal_components