#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  4 14:33:37 2018

@author: unlearn
"""
import os
import cv2
import numpy as np
def crop_images(path):
    cascade_file_src = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(cascade_file_src)
    rootdir = path
    counter = 0
    for subdir, dirs, files in os.walk(rootdir):
#        print(type(files))
        for file1 in files:
            try:
                file_path = os.path.join(subdir, file1)
                if(os.path.isfile(file_path)):
#                    print(file_path)
                    image = cv2.imread(file_path)
                    os.remove(file_path)
                    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    faces = faceCascade.detectMultiScale(gray)
                    
#                    print(file_path)
                    for (x, y, w, h) in faces:
                        crop_img = gray[y:y+h, x:x+w]
                        cv2.imwrite(file_path + '.jpg',crop_img)
            except Exception as e:
                print(e)
                
                continue
            counter += len(files)
    print(counter)
    
def convert_to_grayscale(path,SIZE):
    average_face = np.zeros((SIZE,SIZE))
    counter = 0
    error_counter = 0
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
            try:
                file_path = os.path.join(subdir, file1)
                if(os.path.isfile(file_path) and file_path.endswith('jpg')):
                        print(file_path)
                        image = cv2.imread(file_path)
                        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                        os.remove(file_path)
                        gray_resized = get_square(gray,SIZE)
                        cv2.imwrite(file_path,gray_resized)
                        average_face = average_face + gray_resized 
                        counter = counter + 1
                        print(len(image))
            except:
                error_counter = error_counter + 1
                continue
    print(error_counter)
    average_face = average_face/counter    
    cv2.imwrite('average.jpg', average_face)
    return average_face

def subtract_avg(path, avg, SIZE):
    for subdir, dirs, files in os.walk(path):
        for file1 in files:
            try:
                file_path = os.path.join(subdir, file1)
                if(os.path.isfile(file_path) and file_path.endswith('jpg')):
                    image = cv2.imread(file_path)
                    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    
                    gray = get_square(gray,SIZE)-avg
                    os.remove(file_path)
                    cv2.imwrite(file_path,gray)
            except:
                continue
            
def get_square(image,square_size):

    height,width=image.shape
    if(height>width):
      differ=height
    else:
      differ=width
    differ+=4

    mask = np.zeros((differ,differ), dtype="uint8")   
    x_pos=int((differ-width)/2)
    y_pos=int((differ-height)/2)
    mask[y_pos:y_pos+height,x_pos:x_pos+width]=image[0:height,0:width]
    mask=cv2.resize(image,(square_size,square_size),interpolation=cv2.INTER_LANCZOS4)

    return mask
